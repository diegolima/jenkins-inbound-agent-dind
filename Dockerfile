FROM jenkins/inbound-agent:latest

USER root
RUN apt-get update \
	&& apt-get install -y \
		docker.io \
		sudo \
	&& rm -Rf /var/lib/apt/lists/*

RUN echo "jenkins ALL = (root) NOPASSWD: /usr/bin/docker" >> /etc/sudoers

USER jenkins
